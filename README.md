# Cloudflare workers pastebin serverless server example (with basic login)

This project demonstrates the use of Cloudflare Workers with TypeScript. It provides a simple pastebin-like functionality with support for login, search, delete, and add-edit operations on key-value pairs stored in a KVNamespace.

## Table of Contents

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [Limitations](#limitations)

## Introduction

Cloudflare Workers allow you to run serverless functions at the edge, close to the users, providing low-latency responses to their requests. In this project, we use TypeScript to benefit from type safety and better development experience.

## Requirements

- Node.js (LTS or newer)
- NPM or Yarn
- Cloudflare account with Workers KV enabled
- [Wrangler CLI](https://developers.cloudflare.com/workers/cli-wrangler/install-update) installed and configured

## Installation

1. Clone the repository:

```
git clone https://git.disroot.org/cyi1341/cf-workers-pastebin.git
```

2. Install dependencies:

```
npm install
```

or

```
yarn install
```

3. Configure the `wrangler.toml` file with your  KV namespace id.

4. Build the project:

```
npm run build
```

or

```
yarn build
```

5. Deploy the Worker to your Cloudflare account:

```
wrangler publish
```

## Usage

The following API endpoints are available:

- `POST /login`: Authenticate with a username and password.
  - Request body: `{ "username": "your_username", "password": "your_password" }`
  - Response: `Login successful` or an error message with the corresponding status code.
- `POST /search`: Search for key-value pairs based on key or value.
  - Request body: `{ "key": "search_key", "value": "search_value" }`
  - Response: A JSON array of matching key-value pairs.
- `POST /delete`: Delete a specific key-value pair or multiple pairs based on certain conditions.
  - Request body: Various options are available, such as `{ "individual": "key_to_delete" }`, `{ "search": "search_term" }`, `{ "allBut": "key_to_keep" }`, or `{ "emptyField": true, "confirmation": true }`.
  - Response: A success message or an error message with the corresponding status code.
- `POST /delete-all`: Delete all key-value pairs.
  - Request body: `{ "confirmation": true }`
  - Response: `All entries deleted` or an error message with the corresponding status code if confirmation is missing.
- `POST /add-edit`: Add or edit key-value pairs.
  - Request body: `{ "keyValuePairs": { "key1": "value1", "key2": "value2" }, "replaceOption": "all" }`. The `replaceOption` can be "all" or "choice". If "choice" is used, specify an additional field `"replaceKeys": ["key1"]` to list the keys to replace.
  - Response: A success message or an error message with the corresponding status code.

Refer to the code for more details on the expected request format and response structure for each endpoint.

## Limitations

- The project uses Cloudflare Workers KV for storage, which has certain [limitations](https://developers.cloudflare.com/workers/platform/limits) on the number of read and write operations per day, as well as the maximum size of each value.
- The login functionality is basic and does not include advanced features like token-based authentication or password hashing.
- The search functionality is limited to exact matches for key and value.
