import { KVNamespace } from '@cloudflare/workers-types';

export const PASTEBIN: KVNamespace = (self as any).PASTEBIN;

