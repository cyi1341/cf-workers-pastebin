import { KVNamespace } from '@cloudflare/workers-types'

declare const PASTEBIN: KVNamespace

async function handleRequest(request: Request): Promise<Response> {
  const url = new URL(request.url)
  const path = url.pathname.split('/').filter(p => p.length > 0)

  if (path.length === 0) {
    return new Response('Invalid request', { status: 400 })
  }

  const action = path[0]
  const body = await request.json()

  switch (action) {
    case 'login':
      if (body.username && body.password) {
        const storedUsername = await PASTEBIN.get('username')
        const storedPassword = await PASTEBIN.get('password')
        if (storedUsername === body.username && storedPassword === body.password) {
          return new Response('Login successful', { status: 200 })
        } else {
          return new Response('Invalid username or password', { status: 401 })
        }
      } else {
        return new Response('Missing username or password', { status: 400 })
      }
    case 'search':
      const pastebinData = JSON.parse(await PASTEBIN.get('pastebin') || '[]')
      let results = pastebinData

      if (body.key) {
        results = results.filter((entry: any) => entry.key === body.key)
      }
      if (body.value) {
        results = results.filter((entry: any) => entry.value === body.value)
      }

      return new Response(JSON.stringify(results), { status: 200 })

	case 'delete':
	  const currentData = JSON.parse(await PASTEBIN.get('pastebin') || '[]')
	  let newData = currentData

	  if (body.emptyField) {
		if (body.confirmation) {
		  newData = []
		} else {
		  return new Response('Confirmation required', { status: 400 })
		}
	  } else if (body.individual) {
		newData = currentData.filter((entry: any) => entry.key !== body.individual)
	  } else if (body.search) {
		const searchResults = currentData.filter((entry: any) => entry.key === body.search || entry.value === body.search)
		newData = currentData.filter((entry: any) => !searchResults.includes(entry))
	  } else if (body.allBut) {
		newData = currentData.filter((entry: any) => entry.key !== body.allBut && entry.value !== body.allBut)
	  }

	  await PASTEBIN.put('pastebin', JSON.stringify(newData))
	  return new Response(`Deletion successful for key '${body.individual || ''}'`, { status: 200 })

	case 'delete-all':
	  if (body.confirmation) {
		await PASTEBIN.put('pastebin', JSON.stringify([]))
		return new Response('All entries deleted', { status: 200 })
	  } else {
		return new Response('Confirmation required', { status: 400 })
	  }
      await PASTEBIN.put('pastebin', JSON.stringify(newData))
      return new Response('Deletion successful', { status: 200 })
	case 'add-edit':
	  const existingData = JSON.parse(await PASTEBIN.get('pastebin') || '[]');
	  const inputEntries = Object.entries(body.keyValuePairs).map(([key, value]) => ({ [key]: value }));

	  // Error handling: Check if the input data is valid
	  if (inputEntries.some(entry => Object.keys(entry).length !== 1 || Object.values(entry)[0] === undefined)) {
		return new Response('Invalid input format', { status: 400 });
	  }

	  const updatedData = inputEntries.reduce((acc: any[], keyValue: any) => {
		const key = Object.keys(keyValue)[0];
		const duplicateIndex = acc.findIndex((entry: any) => Object.keys(entry)[0] === key);
		if (duplicateIndex > -1) {
		  if (body.replaceOption === 'all' || (body.replaceOption === 'choice' && body.replaceKeys.includes(key))) {
			acc[duplicateIndex] = keyValue;
		  }
		} else {
		  acc.push(keyValue);
		}
		return acc;
	  }, existingData);

	  await PASTEBIN.put('pastebin', JSON.stringify(updatedData));
	  return new Response('Add/Edit successful', { status: 200 });


  }
}

addEventListener('fetch', (event: FetchEvent) => {
  event.respondWith(handleRequest(event.request))
})

